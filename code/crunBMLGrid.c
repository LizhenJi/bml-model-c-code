
#include<R.h>
#include<stdio.h>
/* step: the total number of steps to be moved*/
void runstep(int *grid, int *step, int *row, int *col,double *bluecar,double *redcar, double *velocity)
  {
    int i,k;/* index of position in the grid*/
    int tot = (*row) * (*col);
    int curpos[tot]; /*initial occupied position as curpos*/
    int newgrid[tot];/*define a newgrid for storing intermediate result*/
    for(i=0;i<tot;i++)  newgrid[i] = grid[i];
    for(k = 0; k<*step; k++)
   {
    /*find current position*/
      for(i=0; i<tot; i++) 
      if(grid[i]!=0) curpos[i]=1;/* indentify occupied position as 1*/
      else curpos[i]=0;

    /* odd step, run blue cars*/
    if( k%2 == 0)
    {
      for(i=0; i < tot; i++)
      {
        if(grid[i]==1)/*run blue cars*/
          { /*position not in first row*/
            if(i%*row!=0)
	    {
               if(curpos[i-1]==0) {newgrid[i-1] = 1; newgrid[i] = 0; velocity[k]++ ;}
	    }
            else /*position in first row*/
	    {
               if(curpos[i+*row-1] == 0) {newgrid[i+*row-1] = 1; newgrid[i] = 0 ;velocity[k]++; }
	    }
          }
       }
     }
    /*even step, run red cars*/
     else
       {
         for(i=0; i < tot; i++)
          {
            if(grid[i]==2)
            { /*position not in last column*/
               if(i<tot-*row)
                 {
                   if(curpos[i+*row]==0) {newgrid[i+*row] = 2; newgrid[i] = 0; velocity[k]++;}
                 }
               else  /*position in last column*/
                {
                   if(curpos[i-(*col-1)*(*row)]==0) {newgrid[i-(*col-1)*(*row)]=2; newgrid[i]=0;velocity[k]++; }
                }
            }
          }
     }
     /*renew grid*/
     for(i=0;i<tot;i++)  grid[i] = newgrid[i];
   }
   /*current velocity save number of moved steps, compute the total velocity */
   for(i=0; i<*step; i++)
       if(i%2==0) velocity[i] = velocity[i]/ *bluecar;
       else velocity[i] = velocity[i]/  *redcar;
  }
